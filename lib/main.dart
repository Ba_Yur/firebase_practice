
import 'package:firebase_practice/auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  // User user = User();

  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        title: Text('Firebase practice'),
      ),
      body: Container(
        child: RaisedButton(
          onPressed: () async {
              await _auth.signIn();
            },
            // () => sendPost(user)
          child: Text('Tap me!'),
        ),
      ),
    );
  }
}
