

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';


class AuthService {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future signIn() async {
    await Firebase.initializeApp();
    try{
      UserCredential result = await _auth.signInAnonymously();
      print('are signed');
      print(result.user);
      return result.user;
    } catch (error) {
      print('here is an error');
      return null;
    }
  }

}